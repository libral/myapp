import React,{Component} from 'react';
import {AppRegistry, Text,View,ScrollView,SafeAreaView,Animated} from 'react-native';
import Card from "./app/components/Card";
import styled from "styled-components";
import LogoComponent from './app/components/LogoComponent'
import LearningComponent from './app/components/LearningComponent'
import TitleComponent  from './app/components/TitleComponent'
import CoursesComponent  from './app/components/CoursesComponent'
import Menu from "./app/components/Menu";



export default class myApp extends Component{



    render (){

        return(




           <SafeAreaView>

               <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                showsVerticalScrollIndicator={false}
            >
            <Container>


                <TitleComponent />
                <Avatar source={require("./assets/avatar.jpg")} />


                    <LogoComponent />
                    <Subtitle>Continue Learning</Subtitle>
                    <LearningComponent />


                        <Subtitle>Popular Courses</Subtitle>
                    <CoursesComponent/>

            </Container>
            </ScrollView>
        </SafeAreaView>



        )
    }
}


const Container = styled.View`
  background: #f0f3f5;
  flex: 1;
`;

const Avatar = styled.Image`
  width: 44px;
  height: 44px;
  background: black;
  border-radius: 22px;
  margin-left: 20px;
  position: absolute;
  top: 0;
  left: 0;
  alignItems:center
`;

const Subtitle = styled.Text`
  color: #b8bece;
  font-weight: 600;
  font-size: 15px;
  margin-left: 20px;
  margin-top: 50px;
  text-transform: uppercase;
`;





AppRegistry.registerComponent('myApp', () => myApp)
