import React,{Component} from 'react';
import {AppRegistry, Text,View} from 'react-native';
import Component1 from './app/components/Component1'

export default class myApp extends Component{

    render (){

        return (
            <View>
                <Component1 message="I am the message"/>
            </View>
        )
    }
}


AppRegistry.registerComponent('myApp', () => myApp)
