import React,{Component } from 'react'

import {Text, View, AppRegistry,TextInput} from 'react-native'
import styled from "styled-components";

import {Text, View, AppRegistry,TextInput,Switch} from 'react-native'

export default class Component3 extends Component{

    constructor(props)
    {
        super(props);
        this.state = {
            text: "Lawrence",
            showName: true,
            message: this.props.message,
            valueChange:false
        }
    }


    static defaultProps = {
        message:"Hi there"
    }

    onChangeText (value){
        this.setState({text:value})
    }

    onSubmit (value){
        this.setState({text:value})
    }


    onValueChange(value){
        this.setState({valueChange:value})
    }



    render (){


        return(


            <View>

                    <TextInput
                        placeholder="ENTER TEXT"
                        value={this.state.text}
                        onChangeText={(value) => this.onChangeText(value)}
                    />



                    <Text>
                        {this.state.text}
                    </Text>


                <Switch value={this.state.valueChange} onValueChange={(value) => this.onValueChange(value)}/>
            </View>








        )
    }



}



const Subtitle = styled.Text`
  color: #b8bece;
  font-weight: 600;
  font-size: 15px;
  margin-left: 20px;
  margin-top: 50px;
  text-transform: uppercase;
`;

const Title = styled.Text`
  font-size: 16px;
  color: #b8bece;
  font-weight: 500;
`;

const Name = styled.Text`
  font-size: 20px;
  color: #3c4560;
  font-weight: bold;
`;

const TitleBar = styled.View`
  width: 100%;
  margin-top: 50px;
  padding-left: 20px;
`;

const Container = styled.View`
  background: #f0f3f5;
  flex: 1;
`;


const Avatar = styled.Image`
  width: 44px;
  height: 44px;
  background: black;
  border-radius: 22px;
  margin-left: 20px;
  position: absolute;
  top: 0;
  left: 0;
  alignItems:center
`;


const styles = {

    myView: {
        backgroundColor: "blue"
    },

    container: {
        flexDirection:'row',
        height:100

    },

    myText: {
        color: "white"
    },


    v1:{
        flex:1,
        backgroundColor:"red",
        padding:10
    },
    v2:{
        flex:1,
        backgroundColor:"green",
        padding:10
    },
    v3:{
        flex:1,
        backgroundColor:"black",
        padding:10
    },

    vText:{
        color:"white"
    }
}

AppRegistry.registerComponent('Component',()=>Component3)
