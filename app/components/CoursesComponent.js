import React,{Component} from "react";
import styled from "styled-components";
import {AppRegistry, SafeAreaView, ScrollView} from 'react-native'
import Course from './Course'



export default class CoursesComponent extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            courses: [

                {
                    title: "Prototype in InVision Studio",
                    subtitle: "10 sections",
                    image: require("../../assets/background13.jpg"),
                    logo: require("../../assets/logo-studio.png"),
                    author: "Meng To",
                    avatar: require("../../assets/avatar.jpg"),
                    caption: "Design an interactive prototype"
                },
                {
                    title: "React for Designers",
                    subtitle: "12 sections",
                    image: require("../../assets/background11.jpg"),
                    logo: require("../../assets/logo-react.png"),
                    author: "Meng To",
                    avatar: require("../../assets/avatar.jpg"),
                    caption: "Learn to design and code a React site"
                },
                {
                    title: "Design and Code with Framer X",
                    subtitle: "10 sections",
                    image: require("../../assets/background14.jpg"),
                    logo: require("../../assets/logo-framerx.png"),
                    author: "Meng To",
                    avatar: require("../../assets/avatar.jpg"),
                    caption: "Create powerful design and code components for your app"
                },
                {
                    title: "Design System in Figma",
                    subtitle: "10 sections",
                    image: require("../../assets/background6.jpg"),
                    logo: require("../../assets/logo-figma.png"),
                    author: "Meng To",
                    avatar: require("../../assets/avatar.jpg"),
                    caption:
                        "Complete guide to designing a site using a collaborative design tool"
                }

            ]
        }
    }

    render() {
        return (


            this.state.courses.map((course,index) => (
                <Course
                    key={index}
                    image={course.image}
                    title={course.title}
                    subtitle={course.subtitle}
                    logo={course.logo}
                    author={course.author}
                    avatar={course.avatar}
                    caption={course.caption}
                />
            ))

        )
    }
}

AppRegistry.registerComponent('CoursesComponent', ()=> CoursesComponent)
