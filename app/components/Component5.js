import React,{Component } from 'react'

import {Text, View, AppRegistry,TextInput} from 'react-native'
import styled from "styled-components";
import Card from './Card'


export default class Component5 extends Component{

    constructor(props)
    {
        super(props);
        this.state = {
            text: "Lawrence",
            showName: true,
            message: this.props.message,
            valueChange:false
        }
    }





    render (){


        return(



            <Container>
                <TitleBar>
                    <Title>Welcome back,</Title>
                    <Name>Libral lawrence</Name>
                </TitleBar>
                <Avatar source={require("../../assets/avatar.jpg")} />
                <Subtitle>Continue Learning</Subtitle>




                <Card
                    title="Styled Components"
                    image={require("../../assets/background2.jpg")}
                    caption="React Native"
                    logo={require("../../assets/logo-react.png")}
                    subtitle="5 of 12 sections"
                />
            </Container>




        )
    }



}



const Subtitle = styled.Text`
  color: #b8bece;
  font-weight: 600;
  font-size: 15px;
  margin-left: 20px;
  margin-top: 50px;
  text-transform: uppercase;
`;

const Title = styled.Text`
  font-size: 16px;
  color: #b8bece;
  font-weight: 500;
`;

const Name = styled.Text`
  font-size: 20px;
  color: #3c4560;
  font-weight: bold;
`;

const TitleBar = styled.View`
  width: 100%;
  margin-top: 50px;
  padding-left: 20px;
`;

const Container = styled.View`
  background: #f0f3f5;
  flex: 1;
`;


const Avatar = styled.Image`
  width: 44px;
  height: 44px;
  background: black;
  border-radius: 22px;
  margin-left: 20px;
  position: absolute;
  top: 0;
  left: 0;
  alignItems:center
`;




AppRegistry.registerComponent('Component',()=>Component5)


//https://designcode.io/react-native-styled-components
