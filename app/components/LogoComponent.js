import React,{Component} from "react";
import styled from "styled-components";
import {AppRegistry, SafeAreaView, ScrollView} from 'react-native'
import Logo from './Logo'


export default class LogoComponent extends Component{

    render(){
        return (
            <ScrollView
                style={{ flexDirection: "row",padding: 20,paddingLeft: 12, paddingTop: 30 }}
                horizontal={true} showsHorizontalScrollIndicator={false}
            >
                <Logo image={require("../../assets/logo-framerx.png")} text="Framer" />
                <Logo image={require("../../assets/logo-figma.png")} text="Figma" />
                <Logo image={require("../../assets/logo-studio.png")} text="Studio" />
                <Logo image={require("../../assets/logo-react.png")} text="React" />
                <Logo image={require("../../assets/logo-swift.png")} text="Swift" />
                <Logo image={require("../../assets/logo-sketch.png")} text="Sketch" />

            </ScrollView>
        )
    }
}


AppRegistry.registerComponent('LogoComponent', ()=> LogoComponent)
