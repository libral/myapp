import React,{Component} from "react";
import styled from "styled-components";
import {AppRegistry, SafeAreaView, ScrollView} from 'react-native'
import Logo from './Logo'
import Card from "./Card";


export default class TitleComponent extends Component{

    render(){
        return (
            <TitleBar>
                <Title>Welcome back,</Title>
                <Name>Libral lawrence</Name>
            </TitleBar>

        )
    }
}

const TitleBar = styled.View`
  width: 100%;
  margin-top: 50px;
  padding-left: 20px;
`;

const Title = styled.Text`
  font-size: 16px;
  color: #b8bece;
  font-weight: 500;
`;

const Name = styled.Text`
  font-size: 20px;
  color: #3c4560;
  font-weight: bold;
`;


AppRegistry.registerComponent('TitleComponent', ()=> TitleComponent)
