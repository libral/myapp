import React,{Component} from "react";
import styled from "styled-components";
import {AppRegistry, SafeAreaView, ScrollView} from 'react-native'
import Logo from './Logo'
import Card from "./Card";


export default class LearningComponent extends Component{

    constructor(props){
        super(props)
        this.state = {
            cards: [
                {
                    title: "React Native for Designers",
                    image: require("../../assets/background11.jpg"),
                    subtitle: "React Native",
                    caption: "1 of 12 sections",
                    logo: require("../../assets/logo-react.png")
                },
                {
                    title: "Styled Components",
                    image: require("../../assets/background12.jpg"),
                    subtitle: "React Native",
                    caption: "2 of 12 sections",
                    logo: require("../../assets/logo-react.png")
                },
                {
                    title: "Props and Icons",
                    image: require("../../assets/background13.jpg"),
                    subtitle: "React Native",
                    caption: "3 of 12 sections",
                    logo: require("../../assets/logo-react.png")
                },
                {
                    title: "Static Data and Loop",
                    image: require("../../assets/background14.jpg"),
                    subtitle: "React Native",
                    caption: "4 of 12 sections",
                    logo: require("../../assets/logo-react.png")
                }

            ]
        }
    }
    render(){
        return (
            <ScrollView horizontal={true} style={{ paddingBottom: 30 }} showsHorizontalScrollIndicator={false}>
                { this.state.cards.map((card,index) => (
                <Card
                    key={index}
                    title={card.title}
                    image={card.image}
                    caption={card.caption}
                    logo={card.logo}
                    subtitle={card.subtitle}
                />
                ))}
            </ScrollView>
        )
    }
}


AppRegistry.registerComponent('LearningComponent', ()=> LearningComponent)
