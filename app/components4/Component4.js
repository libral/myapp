import React,{Component } from 'react'
import {Text, View, AppRegistry,TextInput,Switch,FlatList,SectionList,Button} from 'react-native'


export default class Component4 extends Component{


    render (){


        return(


            <View>
                     <FlatList data=
                                   {[
                                       {name:'Lawrence'},
                                       {name:'Janiel'},
                                       {name:'Benny'},
                                       {name:'Buyola'},
                                       {name:'Abaed'}

                                       ]}
                               renderItem={({item}) => <Text style={styles.item} >{item.name}</Text>}
                     />

                <Text style={{padding:10,backgroundColor:"black",color:"white"}}>DIVIDE</Text>


                     <SectionList
                        sections={[
                            { title:"Names", data:['Lawrence','Daniel','benkaya']},
                            { title:"Cars", data:['Toyota','Benz']}

                        ]}

                        renderItem={({item}) => <Text style={styles.item}>{item}</Text>}
                        renderSectionHeader = {({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                        keyExtractor = {(item,index) => index}
                     />

                <Button

                    title="Learn More"
                    color="#841584"
                    accessibilityLabel="Learn button"
                />
            </View>








        )
    }



}


const styles = {

    sectionHeader: {
        paddingTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 2,
        fontSize: 14,
        fontWeight: 'bold',
        backgroundColor: 'grey',
        color:'white'
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    myView: {
        backgroundColor: "blue"
    },

    container: {
        flexDirection:'row',
        height:100

    },

    myText: {
        color: "white"
    },


    v1:{
        flex:1,
        backgroundColor:"red",
        padding:10
    },
    v2:{
        flex:1,
        backgroundColor:"green",
        padding:10
    },
    v3:{
        flex:1,
        backgroundColor:"black",
        padding:10
    },

    vText:{
        color:"white"
    }
}

AppRegistry.registerComponent('Component',()=>Component4)
