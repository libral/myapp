import React,{Component } from 'react'
import {Text, View, AppRegistry,TouchableHighlight,TouchableOpacity,Image} from 'react-native'


export default class Component2 extends Component{

    constructor(props)
    {
        super(props);
        this.state = {
            name: "Lawrence",
            showName: true,
            message: this.props.message
        }
    }


    static defaultProps = {
        message:"Hi there"
    }

    onPress(){

        console.log('I am pressed')
    }

    onPress2(){

        console.log('I am pressed area2')
    }


    render (){



        let name = this.state.showName ? this.state.name : "no namse"
        let pic = {
            uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
        };


        return(

            <View >
                <View style={styles.myView}>
                    <Text style={{color:'white'}}>Welcome {name}</Text>
                </View>

                <View>
                    <Image source={pic} style={{width: 193, height: 110}}/>
                </View>


                <View style={styles.container}>

                    <TouchableHighlight
                        style={styles.v1}
                        onPress={this.onPress}
                        underlayColor="red"

                    >


                        <View >
                            <Text> View 1onw</Text>

                        </View>
                    </TouchableHighlight>

                    <TouchableOpacity
                        style={styles.v2}
                        onPress={this.onPress2}
                    >
                    <View >
                        <Text> View 2</Text>

                    </View>
                    </TouchableOpacity>

                    <View style={styles.v3}>
                        <Text style={styles.vText} > View 3</Text>

                    </View>

                </View>

            </View>



        )
    }



}


const styles = {

    myView: {
        backgroundColor: "blue"
    },

    container: {
        flexDirection:'row',
        height:100

    },

    myText: {
        color: "white"
    },


    v1:{
        flex:1,
        backgroundColor:"red",
        padding:10
    },
    v2:{
        flex:1,
        backgroundColor:"green",
        padding:10
    },
    v3:{
        flex:1,
        backgroundColor:"black",
        padding:10
    },

    vText:{
        color:"white"
    }
}

AppRegistry.registerComponent('Component',()=>Component2)
